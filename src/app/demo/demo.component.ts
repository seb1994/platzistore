import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit {

  constructor() { }

  title = 'platzi-store';
  newItem = '';
  power = 10;

  items = ['Manzana', 'Banano', 'Pera', 'Mandarina'];

  // tslint:disable-next-line: typedef
  addItem(nItem){
    this.items.push(nItem);
    this.newItem = '';
  }

  // tslint:disable-next-line: typedef
  deleteItem(index: number){
    this.items.splice(index, 1);
  }

  ngOnInit(): void {
  }

}
